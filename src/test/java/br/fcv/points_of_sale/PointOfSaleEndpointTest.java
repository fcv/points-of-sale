package br.fcv.points_of_sale;

import static br.fcv.points_of_sale.SampleDataCLR.openingHour;
import static br.fcv.points_of_sale.SampleDataCLR.time;
import static com.google.common.collect.Sets.newHashSet;
import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.DayOfWeek.THURSDAY;
import static java.time.DayOfWeek.TUESDAY;
import static java.time.DayOfWeek.WEDNESDAY;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "guest", password = "guest")
public class PointOfSaleEndpointTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private PointOfSaleRepository repository;

	@Autowired
	private UserService userService;

	private PointOfSale manhattan;

	@Before
	public void prepareData() {

		repository.deleteAll();

		manhattan = repository.save(new PointOfSale( //
				"Manhattan 44", //
				"(48) 3028-4405", //
				new Address( //
						"R. Des. Vítor Lima", //
						"410", //
						"Trindade", //
						"Florianópolis", //
						"Santa Catarina", //
						"Brasil", //
						"88040-401" //
				), //
				newHashSet( //
						openingHour(MONDAY, time(7, 30), time(10, 0)), //
						openingHour(TUESDAY, time(7, 30), time(10, 0)), //
						openingHour(WEDNESDAY, time(7, 30), time(10, 0)), //
						openingHour(THURSDAY, time(7, 30), time(10, 0)), //
						openingHour(FRIDAY, time(7, 30), time(10, 0)), //
						openingHour(SATURDAY, time(7, 30), time(10, 0)), //
						openingHour(SUNDAY, time(7, 30), time(10, 0)) //
				)//
				));
	}

	@Test
	public void testEntityJsonFormat() throws Exception {

		Long id = manhattan.getId();

		mvc.perform(
				get("/api/rest/v1/points-of-sale/{id}", id).accept(
						APPLICATION_JSON_UTF8))
				.andExpect(status().isOk())
				.andExpect(
						content().contentTypeCompatibleWith(APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is("Manhattan 44")))
				.andExpect(jsonPath("$.phoneNumber", is("(48) 3028-4405")))
				.andExpect(
						jsonPath("$.address.streetName",
								is("R. Des. Vítor Lima")))
				.andExpect(jsonPath("$.address.number", is("410")))
				.andExpect(jsonPath("$.address.neighborhood", is("Trindade")))
				.andExpect(jsonPath("$.address.city", is("Florianópolis")))
				.andExpect(jsonPath("$.address.state", is("Santa Catarina")))
				.andExpect(jsonPath("$.address.country", is("Brasil")))
				.andExpect(jsonPath("$.address.zipCode", is("88040-401")))
				.andExpect(jsonPath("$.openingHours", hasSize(7)))
				.andExpect(
						jsonPath(
								"$.openingHours[*].dayOfWeek",
								containsInAnyOrder("MONDAY", "TUESDAY",
										"WEDNESDAY", "THURSDAY", "FRIDAY",
										"SATURDAY", "SUNDAY")))
				.andExpect(
						jsonPath("$.openingHours[*].start",
								everyItem(is("07:30:00"))))
				.andExpect(
						jsonPath("$.openingHours[*].end",
								everyItem(is("10:00:00"))));
	}

	@Test
	public void testSingleEntityNotFound() throws Exception {

		Long nonexistentId = 666L;
		mvc.perform(
				get("/api/rest/v1/points-of-sale/{id}", nonexistentId).accept(
						APPLICATION_JSON_UTF8))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testSingleEntityDelete() throws Exception {

		Long id = manhattan.getId();
		mvc.perform(delete("/api/rest/v1/points-of-sale/{id}", id).with(csrf())).andExpect(
				status().isNoContent());
		mvc.perform(
				get("/api/rest/v1/points-of-sale/{id}", id).accept(
						APPLICATION_JSON_UTF8))
				.andExpect(status().isNotFound());
	}
}