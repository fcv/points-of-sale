$(function() {

	function registerPersistentTabBehavior() {

		// implementation based on http://stackoverflow.com/a/21443271/301686
		// show active tab on reload
		if (location.hash !== '')
			$('a[href="' + location.hash + '"]').tab('show');

		// remember the hash in the URL without jumping
		$('a[data-toggle="tab"]').on(
				'shown.bs.tab',
				function(e) {
					var hashValue = $(e.target).attr('href');
					if (history.pushState) {
						history.pushState(null, null, hashValue);
					} else {
						location.hash = hashValue;
					}
				});
	};

	registerPersistentTabBehavior();
});
