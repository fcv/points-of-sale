$(function($) {

	var $pointsOfSaleTable = $('.points-of-sale-result-table');

	function renderList(pointsOfSale) {
		var $tbody = $('tbody', $pointsOfSaleTable);

		var template = $('#point-of-sale-row-tmpl').html();
		Mustache.parse(template);
		var rendered = Mustache.render(template, pointsOfSale);
		$tbody
			.empty()
			.append(rendered);
	}

	function refreshList() {

		var endpointUrl = $pointsOfSaleTable.data('endpoint-url');

		$.get(endpointUrl)
			.done(function(data) {

				var pointsOfSale = data._embedded || data,
					pointsOfSale = pointsOfSale.pointsOfSale || pointsOfSale;

				renderList(pointsOfSale);
			});
	}

	function remove(evt) {

		evt.preventDefault();

		var promise = null;
		if (confirm('Are you sure?')) {

			var $trigger = $(this),
				endpointUrl = $trigger.data('endpoint-url');

			promise = $.ajax({
				method: 'DELETE',
				url: endpointUrl,
				dataType: 'json',
			});

			promise.done(function() {
				$trigger.closest('tr').remove();
			});
		}
		return promise;
	}

	function init() {

		refreshList();
		$pointsOfSaleTable.on('click', '.remove-trigger', remove);
		$(document).on('point-of-sale-saved', function() {
			refreshList();
		});
	}

	init();
});

$(function($) {

	var $modal = $('#point-of-sale-modal'),
		$openingHoursPanel = $('.opening-hours-panel', $modal),
		$form = $('form', $modal),
		validator = $form.validate()
		;

	function resetForm() {

		//-- reset jquery-validate
		validator.resetForm();

		$form.each(function() {
			this.reset();
		});

		removeOpeningHourLines();
	}

	function save() {

		var isValid = validator.form();
		if (!isValid) {
			return;
		}

		var name = $('[name=name]', $form).val();
		var phoneNumber = $('[name=phoneNumber]', $form).val();
		var address = {
			streetName: $('[name="address.streetName"]', $form).val(),
			number: $('[name="address.number"]', $form).val(),
			neighborhood: $('[name="address.neighborhood"]', $form).val(),
			city: $('[name="address.city"]', $form).val(),
			state: $('[name="address.state"]', $form).val(),
			country: $('[name="address.country"]', $form).val(),
			zipCode: $('[name="address.zipCode"]', $form).val()
		};

		var openingHours = $('.opening-hours-container .row', $openingHoursPanel)
			.map(function() {
				var $row = $(this);
				return {
					dayOfWeek: $('[name=dayOfWeek]', $row).val(),
					start: $('[name=start]', $row).val(),
					end: $('[name=end]', $row).val()
				};
			})
			.toArray()
			.filter(function(openingHour) {
				return openingHour.dayOfWeek != null &&
					openingHour.start != null && openingHour.end != null;
			});

		var pointOfSale = {
			name: name,
			phoneNumber: phoneNumber,
			address: address,
			openingHours: openingHours
		}

		var endpointUrl = $form.data('endpoint-url');
		var method = 'POST';

		return $.ajax({
			method: method,
			url: endpointUrl,
			data: JSON.stringify(pointOfSale),
			contentType: 'application/json',
			dataType: 'json',
			success: function() {

				$modal.modal('hide');
				$modal.trigger('point-of-sale-saved');
			}
		});
	}

	function addOpeningHourLine() {

		var template = $('#opening-hour-row-tmpl').html();
		Mustache.parse(template);
		var rendered = Mustache.render(template);

		$('.opening-hours-container', $modal)
			.append(rendered);
	}

	function removeOpeningHourLines() {
		$('.opening-hours-container .row:not(:first)', $modal).remove();
	}

	function removeOpeningHourLine(evt) {

		evt.preventDefault();
		var $trigger = $(this)
			$line = $trigger.closest('.row')
			;

		$line.remove();
	}

	function init() {
		// modal events
		$modal.on('hidden.bs.modal', resetForm);
		$('.save-trigger', $modal).click(save);

		// opening hours events
		$('.add-opening-hour-line-trigger', $openingHoursPanel).click(addOpeningHourLine);
		$('.opening-hours-container', $openingHoursPanel).on('click', '.remove-trigger', removeOpeningHourLine);
	}

	init();
});

// configures AJAX + CSRF
// see more in
// https://docs.spring.io/spring-security/site/docs/4.1.3.RELEASE/reference/html/csrf.html#csrf-include-csrf-token-ajax
$(function ($) {
	var token = $("meta[name='_csrf']").attr("content"),
		header = $("meta[name='_csrf_header']").attr("content");

	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
});
