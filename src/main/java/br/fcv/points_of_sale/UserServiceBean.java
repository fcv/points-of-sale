package br.fcv.points_of_sale;

import static java.lang.String.format;

import org.slf4j.Logger;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class UserServiceBean implements UserService {

	private final Logger logger;
	private final UserResitory repository;
	private final PasswordEncoder passwordEncoder;

	public UserServiceBean(Logger logger, UserResitory repository, PasswordEncoder passwordEncoder) {
		this.logger = logger;
		this.repository = repository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public User signUp(String username, String password) {
		logger.debug("signIn(username: {}, password: <<omitted>>)", username);

		if (repository.countByUsername(username) > 0) {
			throw new UsernameAlreadyTakenException(format("Username '%s' has already been taken", username));
		}
		password = passwordEncoder.encode(password);
		User user = new User(username, password);
		return repository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		logger.debug("findByUsername(username: {})", username);

		return repository.findByUsername(username);
	}
}
