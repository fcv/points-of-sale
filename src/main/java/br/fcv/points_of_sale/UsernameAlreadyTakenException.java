package br.fcv.points_of_sale;

/**
 * Thrown when an attempt to register a new {@link User} fails because its
 * {@link User#getUsername() username} has already been registered by another
 * user
 *
 */
public class UsernameAlreadyTakenException extends RuntimeException {

	private static final long serialVersionUID = 3568235581002636804L;

	public UsernameAlreadyTakenException() {
		super();
	}

	public UsernameAlreadyTakenException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsernameAlreadyTakenException(String message) {
		super(message);
	}

	public UsernameAlreadyTakenException(Throwable cause) {
		super(cause);
	}

}
