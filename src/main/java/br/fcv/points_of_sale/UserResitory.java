package br.fcv.points_of_sale;

import org.springframework.data.repository.CrudRepository;

public interface UserResitory extends CrudRepository<User, Long> {

	public User findByUsername(String username);

	public int countByUsername(String username);

}
