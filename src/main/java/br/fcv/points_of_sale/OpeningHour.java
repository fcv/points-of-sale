package br.fcv.points_of_sale;

import static java.lang.String.format;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class OpeningHour {

	@Column(name = "day_of_week", nullable = false)
	@NotNull
	private DayOfWeek dayOfWeek;

	@Column(name = "start", nullable = false)
	@NotNull
	private LocalTime start;

	@Column(name = "end", nullable = false)
	@NotNull
	private LocalTime end;

	public OpeningHour() {
	}

	public OpeningHour(DayOfWeek dayOfWeek, LocalTime start, LocalTime end) {
		this.dayOfWeek = dayOfWeek;
		this.start = start;
		this.end = end;
	}

	public DayOfWeek getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public LocalTime getStart() {
		return start;
	}

	public void setStart(LocalTime start) {
		this.start = start;
	}

	public LocalTime getEnd() {
		return end;
	}

	public void setEnd(LocalTime end) {
		this.end = end;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dayOfWeek == null) ? 0 : dayOfWeek.hashCode());
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean equals = false;
		if (obj instanceof OpeningHour) {

			OpeningHour other = (OpeningHour) obj;
			equals = Objects.equals(this.dayOfWeek, other.dayOfWeek) &&
					Objects.equals(this.start, other.start) &&
					Objects.equals(this.end, other.end);
		}
		return equals;
	}

	@Override
	public String toString() {
		return format("{dayOfWeek: %s, start: %s, end: %s}", dayOfWeek, start,
				end);
	}

}
