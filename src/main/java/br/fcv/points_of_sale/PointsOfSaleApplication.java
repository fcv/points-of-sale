package br.fcv.points_of_sale;

import static java.lang.String.format;
import static java.util.Collections.emptyList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class PointsOfSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(PointsOfSaleApplication.class, args);
	}

	/**
	 * Spring Factory method for SLF4J {@link Logger} instances
	 *
	 */
	@Bean
	public Logger getLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(ip.getMember().getDeclaringClass());
	}

	@Configuration
	public static class SpringMvcConfig extends WebMvcConfigurerAdapter {

		@Override
		public void addViewControllers(ViewControllerRegistry registry) {
			registry.addViewController("/").setViewName("home");
			registry.addViewController("/login").setViewName("login");
		}
	}

	@Configuration
	public static class SpringSecurityConfig extends
			WebSecurityConfigurerAdapter {

		private final Logger logger;
		
		public SpringSecurityConfig(Logger logger) {
			this.logger = logger;
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {

			// configures HTTP Basic authentication
			// TODO: consider configuring HTTP Digest ..
			// one possible implementation might be based on
			// http://stackoverflow.com/questions/18214296/digest-auth-with-spring-security-using-javaconfig
			http.httpBasic();

			http.authorizeRequests()
					// grant access to JS, .ico files to any request
					.antMatchers("/**/*.js", "/**/*.ico").permitAll()
					// grant access to /signup page for anyone
					.antMatchers("/signup").permitAll()
					// any other request must be authenticated
					.anyRequest().authenticated();

			http.formLogin() //
				.loginPage("/login").permitAll();

			http.logout()
				.permitAll();
		}

		@Bean
		public PasswordEncoder newPasswordEncoder() {
			return new BCryptPasswordEncoder();
		}

		@Bean
		public UserDetailsService newUserDetailsService(UserService userService) {
			return (username) -> {

				logger.debug("loadUserByUsername(username: {})", username);

				User user = userService.findByUsername(username);
				if (user == null) {
					throw new UsernameNotFoundException(format(
							"No user found with username '%s'", username));
				}

				String password = user.getPassword();
				return new org.springframework.security.core.userdetails.User(
						username, password, emptyList());
			};
		}

	}
}
