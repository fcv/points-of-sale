package br.fcv.points_of_sale;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/signup")
public class SignUpController {

	private final UserService userService;

	private final UserDetailsService userDetailsService;

	private final AuthenticationManager authenticationManager;

	public SignUpController(UserService userService, UserDetailsService userDetailsService, AuthenticationManager authenticationManager) {
		this.userService = requireNonNull(userService, "userService cannot be null");
		this.userDetailsService = requireNonNull(userDetailsService, "userDetailsService cannot be null");
		this.authenticationManager = requireNonNull(authenticationManager, "authenticationManager cannot be null");
	}

	@RequestMapping(method = POST)
	public String signUp(@RequestParam String username, @RequestParam String password, RedirectAttributes redirectAttributes) {

		try {
			userService.signUp(username, password);
		} catch (UsernameAlreadyTakenException ex) {

			// TODO: Consider externalize error messages into a properties file
			redirectAttributes.addFlashAttribute("errors", asList("Username already taken"));
			return "redirect:/login#signup-panel";
		}

		// automatically log new user in
		// implementation based on http://stackoverflow.com/questions/10763620/spring-security-login-automatically-after-user-creation/18197510#18197510
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
		authenticationManager.authenticate(auth);
		if(auth.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
		return "redirect:/";
	}
}
