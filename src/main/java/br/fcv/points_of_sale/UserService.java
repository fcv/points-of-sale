package br.fcv.points_of_sale;

public interface UserService {

	public User findByUsername(String username);

	public User signUp(String username, String password);

}
