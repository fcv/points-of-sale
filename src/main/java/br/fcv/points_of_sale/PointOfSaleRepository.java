package br.fcv.points_of_sale;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "pointsOfSale", path = "points-of-sale")
public interface PointOfSaleRepository extends CrudRepository<PointOfSale, Long> {

}
