package br.fcv.points_of_sale;

import static java.lang.String.format;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

	private String streetName;
	private String number;
	private String neighborhood;
	private String city;
	private String state;
	private String country;
	private String zipCode;

	public Address() {
	}

	public Address(String streetName, String number, String neighborhood,
			String city, String state, String country, String zipCode) {
		this.streetName = streetName;
		this.number = number;
		this.neighborhood = neighborhood;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return format(
				"{streetName: %s, number: %s, neighborhood: %s, city: %s, state: %s, country: %s, zipCode: %s}",
				streetName, number, neighborhood, city, state, country, zipCode);
	}

}
