package br.fcv.points_of_sale;

import static java.lang.String.format;
import static javax.persistence.GenerationType.AUTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
// aims to avoid name "conflicts" on PostgreSQL's internal function 'user' and
// our own 'user' table
// see http://dba.stackexchange.com/a/75559
@Table(name = "auth_user")
public class User {

	@Id
	@GeneratedValue(strategy = AUTO)
	private Long id;

	@Column(nullable = false, unique = true)
	@NotNull
	private String username;

	@Column(nullable = false)
	@NotNull
	private String password;

	public User() {
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return format("{id: %s, username: %s, password: %s}", id, username,
				password);
	}

}
