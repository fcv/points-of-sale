package br.fcv.points_of_sale;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.DayOfWeek.THURSDAY;
import static java.time.DayOfWeek.TUESDAY;
import static java.time.DayOfWeek.WEDNESDAY;

import java.time.DayOfWeek;
import java.time.LocalTime;

import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Insert sample {@link PointOfSale} records during startup.
 * 
 * @author veronez
 *
 */
@Component
public class SampleDataCLR implements CommandLineRunner {

	private final Logger logger;
	private final PointOfSaleRepository repository;
	private final UserService userService;

	public SampleDataCLR(Logger logger, PointOfSaleRepository repository, UserService userService) {
		this.logger = logger;
		this.repository = repository;
		this.userService = userService;
	}

	@Override
	public void run(String... args) throws Exception {

		User guest = userService.signUp("guest", "guest");
		logger.info("stored guest User: {}", guest);

		PointOfSale manhattan = new PointOfSale( //
				"Manhattan 44", //
				"(48) 3028-4405", //
				new Address( //
						"R. Des. Vítor Lima", //
						"410", //
						"Trindade", //
						"Florianópolis", //
						"Santa Catarina", //
						"Brasil", //
						"88040-401" //
				), //
				newHashSet( //
						openingHour(MONDAY, time(7, 30), time(10, 0)), //
						openingHour(TUESDAY, time(7, 30), time(10, 0)), //
						openingHour(WEDNESDAY, time(7, 30), time(10, 0)), //
						openingHour(THURSDAY, time(7, 30), time(10, 0)), //
						openingHour(FRIDAY, time(7, 30), time(10, 0)), //
						openingHour(SATURDAY, time(7, 30), time(10, 0)), //
						openingHour(SUNDAY, time(7, 30), time(10, 0)) //
				) //
		);

		PointOfSale rangosDoFarol = new PointOfSale( //
				"Rangos do Farol", //
				"(48) 3233-0401", //
				new Address( //
						"Av. Me. Benvenuta", //
						"196", //
						"Trindade", //
						"Florianópolis", //
						"Santa Catarina", //
						"Brasil", //
						"88036-500" //
				), //
				newHashSet( //
						openingHour(MONDAY, time(18, 0), time(2, 0)), //
						openingHour(TUESDAY, time(18, 0), time(2, 0)), //
						openingHour(WEDNESDAY, time(18, 0), time(2, 0)), //
						openingHour(THURSDAY, time(18, 0), time(2, 0)), //
						openingHour(FRIDAY, time(18, 0), time(2, 0)), //
						openingHour(SATURDAY, time(18, 0), time(2, 0)), //
						openingHour(SUNDAY, time(18, 0), time(0, 0)) //
				) //
		);

		Iterable<PointOfSale> pointsOfSale = repository.save(newArrayList(
				manhattan, rangosDoFarol));

		logger.info("stored Points of Sale: {}", pointsOfSale);
	}

	//
	// support factory methods
	//
	public static LocalTime time(int a, int b) {
		return LocalTime.of(a, b);
	}

	public  static OpeningHour openingHour(DayOfWeek monday, LocalTime start,
			LocalTime end) {
		return new OpeningHour(monday, start, end);
	}

}
