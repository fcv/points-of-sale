package br.fcv.points_of_sale;

import static java.lang.String.format;
import static javax.persistence.GenerationType.AUTO;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "point_of_sale")
public class PointOfSale {

	@Id
	@GeneratedValue(strategy = AUTO)
	private Long id;

	@Column(nullable = false)
	@NotNull
	@Size(min = 1)
	private String name;

	private String phoneNumber;

	@Embedded
	private Address address;

	@ElementCollection
	@CollectionTable(name = "opening_hour", joinColumns = @JoinColumn(name = "point_of_sale_id"))
	private Set<OpeningHour> openingHours;

	public PointOfSale() {
	}

	public PointOfSale(String name, String phoneNumber, Address address,
			Set<OpeningHour> openingHours) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.openingHours = openingHours;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<OpeningHour> getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(Set<OpeningHour> openingHours) {
		this.openingHours = openingHours;
	}

	@Override
	public String toString() {
		return format(
				"{id: %s, name: %s, phoneNumber: %s, address: %s, openingHours: %s}",
				id, name, phoneNumber, address, openingHours);
	}

}
