# Points of Sale

Simple Web Project used to register and retrieve `Points of Sale` (*"Pontos de Venda"*, *PDV*, in Portuguese) records.

# Getting Started

Project requires Java 8 and it's built using [Maven 3](http://maven.apache.org/). So it can be packaged using command:

    $ mvn package

It is developed over [Spring Boot](https://projects.spring.io/spring-boot/) so one option to run it is using Spring Boot's maven plugin command:

    $ mvn spring-boot:run

This command will start an embedded [Tomcat](http://tomcat.apache.org/) instance and deploy project in it.

By default system uses an in-memory [H2](http://www.h2database.com/) database and it has some [Data automatically stored](src/main/java/br/fcv/points_of_sale/SampleDataCLR.java) during startup, including a guest [User](src/main/java/br/fcv/points_of_sale/User.java) whose `username` and `password` are "guest".

# REST API

Rest API is exposed through URL [http://localhost:8080/api/rest/v1/](http://localhost:8080/api/rest/v1/).

Thus `Points Of Sale` may be retrieved like bellow:

    $ curl -X GET -u guest:guest "http://localhost:8080/api/rest/v1/points-of-sale"
    {
      "_embedded" : {
        "pointsOfSale" : [ {
          "name" : "Manhattan 44",
          "phoneNumber" : "(48) 3028-4405",
          "address" : {
            "streetName" : "R. Des. Vítor Lima",
            "number" : "410",
            "neighborhood" : "Trindade",
            "city" : "Florianópolis",
            "state" : "Santa Catarina",
            "country" : "Brasil",
            "zipCode" : "88040-401"
          },
          "openingHours" : [ {
            "dayOfWeek" : "MONDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "SATURDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "TUESDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "WEDNESDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "FRIDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "SUNDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          }, {
            "dayOfWeek" : "THURSDAY",
            "start" : "07:30:00",
            "end" : "10:00:00"
          } ],
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/points-of-sale/1"
            },
            "pointOfSale" : {
              "href" : "http://localhost:8080/api/rest/v1/points-of-sale/1"
            }
          }
        }, {
          "name" : "Rangos do Farol",
          "phoneNumber" : "(48) 3233-0401",
          "address" : {
            "streetName" : "Av. Me. Benvenuta",
            "number" : "196",
            "neighborhood" : "Trindade",
            "city" : "Florianópolis",
            "state" : "Santa Catarina",
            "country" : "Brasil",
            "zipCode" : "88036-500"
          },
          "openingHours" : [ {
            "dayOfWeek" : "MONDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "SATURDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "WEDNESDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "THURSDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "FRIDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "TUESDAY",
            "start" : "18:00:00",
            "end" : "02:00:00"
          }, {
            "dayOfWeek" : "SUNDAY",
            "start" : "18:00:00",
            "end" : "00:00:00"
          } ],
          "_links" : {
            "self" : {
              "href" : "http://localhost:8080/api/rest/v1/points-of-sale/2"
            },
            "pointOfSale" : {
              "href" : "http://localhost:8080/api/rest/v1/points-of-sale/2"
            }
          }
        } ]
      },
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/points-of-sale"
        },
        "profile" : {
          "href" : "http://localhost:8080/api/rest/v1/profile/points-of-sale"
        }
      }
    }

And a `Point Of Sale` may be created using a POST request:

    curl -i -X POST \
    > -u guest:guest \
    > -H "Content-Type:application/json" -H "Accept: application/json" \
    > -d '{"name":"Test","phoneNumber":"(48) 3028-4405","address":{"streetName":"R. Des. Vítor Lima","number":"410","neighborhood":"Trindade","city":"Florianópolis","state":"Santa Catarina","country":"Brasil","zipCode":"88040-401"},"openingHours":[{"dayOfWeek":"WEDNESDAY","start":"04:15","end":"09:10"},{"dayOfWeek":"SATURDAY","start":"07:30:00","end":"10:00:00"}]}' \
    > "http://localhost:8080/api/rest/v1/points-of-sale"
    HTTP/1.1 201 
    X-Application-Context: application
    Location: http://localhost:8080/api/rest/v1/points-of-sale/3
    Content-Type: application/json;charset=UTF-8
    Transfer-Encoding: chunked
    Date: Wed, 07 Dec 2016 13:59:27 GMT
    
    {
      "name" : "Test",
      "phoneNumber" : "(48) 3028-4405",
      "address" : {
        "streetName" : "R. Des. Vítor Lima",
        "number" : "410",
        "neighborhood" : "Trindade",
        "city" : "Florianópolis",
        "state" : "Santa Catarina",
        "country" : "Brasil",
        "zipCode" : "88040-401"
      },
      "openingHours" : [ {
        "dayOfWeek" : "SATURDAY",
        "start" : "07:30:00",
        "end" : "10:00:00"
      }, {
        "dayOfWeek" : "WEDNESDAY",
        "start" : "04:15:00",
        "end" : "09:10:00"
      } ],
      "_links" : {
        "self" : {
          "href" : "http://localhost:8080/api/rest/v1/points-of-sale/3"
        },
        "pointOfSale" : {
          "href" : "http://localhost:8080/api/rest/v1/points-of-sale/3"
        }
      }
    }